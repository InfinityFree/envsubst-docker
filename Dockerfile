FROM alpine:3

RUN apk add --no-cache envsubst

ENTRYPOINT ["/usr/bin/envsubst"]
